<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes();
Route::get('userVerification/{token}', 'UserVerificationController@approve')->name('userVerification');
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('user-alerts/read', 'UserAlertsController@read');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Product Categories
    Route::delete('product-categories/destroy', 'ProductCategoryController@massDestroy')->name('product-categories.massDestroy');
    Route::post('product-categories/media', 'ProductCategoryController@storeMedia')->name('product-categories.storeMedia');
    Route::post('product-categories/ckmedia', 'ProductCategoryController@storeCKEditorImages')->name('product-categories.storeCKEditorImages');
    Route::resource('product-categories', 'ProductCategoryController');

    // Product Tags
    Route::delete('product-tags/destroy', 'ProductTagController@massDestroy')->name('product-tags.massDestroy');
    Route::resource('product-tags', 'ProductTagController');

    // Products
    Route::delete('products/destroy', 'ProductController@massDestroy')->name('products.massDestroy');
    Route::post('products/media', 'ProductController@storeMedia')->name('products.storeMedia');
    Route::post('products/ckmedia', 'ProductController@storeCKEditorImages')->name('products.storeCKEditorImages');
    Route::resource('products', 'ProductController');

    // Audit Logs
    Route::resource('audit-logs', 'AuditLogsController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);

    // User Alerts
    Route::delete('user-alerts/destroy', 'UserAlertsController@massDestroy')->name('user-alerts.massDestroy');
    Route::resource('user-alerts', 'UserAlertsController', ['except' => ['edit', 'update']]);

    // Content Categories
    Route::delete('content-categories/destroy', 'ContentCategoryController@massDestroy')->name('content-categories.massDestroy');
    Route::resource('content-categories', 'ContentCategoryController');

    // Content Tags
    Route::delete('content-tags/destroy', 'ContentTagController@massDestroy')->name('content-tags.massDestroy');
    Route::resource('content-tags', 'ContentTagController');

    // Content Pages
    Route::delete('content-pages/destroy', 'ContentPageController@massDestroy')->name('content-pages.massDestroy');
    Route::post('content-pages/media', 'ContentPageController@storeMedia')->name('content-pages.storeMedia');
    Route::post('content-pages/ckmedia', 'ContentPageController@storeCKEditorImages')->name('content-pages.storeCKEditorImages');
    Route::resource('content-pages', 'ContentPageController');

    // Menus
    Route::delete('menus/destroy', 'MenuController@massDestroy')->name('menus.massDestroy');
    Route::resource('menus', 'MenuController');

    // Orders
    Route::delete('orders/destroy', 'OrderController@massDestroy')->name('orders.massDestroy');
    Route::resource('orders', 'OrderController', ['except' => ['create', 'store']]);

    // Carts
    Route::delete('carts/destroy', 'CartController@massDestroy')->name('carts.massDestroy');
    Route::resource('carts', 'CartController', ['except' => ['create', 'store']]);

    // Types
    Route::delete('types/destroy', 'TypeController@massDestroy')->name('types.massDestroy');
    Route::resource('types', 'TypeController');

    // Attributes
    Route::delete('attributes/destroy', 'AttributeController@massDestroy')->name('attributes.massDestroy');
    Route::resource('attributes', 'AttributeController');

    // Languages
    Route::delete('languages/destroy', 'LanguageController@massDestroy')->name('languages.massDestroy');
    Route::post('languages/media', 'LanguageController@storeMedia')->name('languages.storeMedia');
    Route::post('languages/ckmedia', 'LanguageController@storeCKEditorImages')->name('languages.storeCKEditorImages');
    Route::resource('languages', 'LanguageController');

    // Product Languages
    Route::delete('product-languages/destroy', 'ProductLanguageController@massDestroy')->name('product-languages.massDestroy');
    Route::post('product-languages/media', 'ProductLanguageController@storeMedia')->name('product-languages.storeMedia');
    Route::post('product-languages/ckmedia', 'ProductLanguageController@storeCKEditorImages')->name('product-languages.storeCKEditorImages');
    Route::resource('product-languages', 'ProductLanguageController');

    // Sale Offers
    Route::delete('sale-offers/destroy', 'SaleOffersController@massDestroy')->name('sale-offers.massDestroy');
    Route::resource('sale-offers', 'SaleOffersController');

    // Taxes
    Route::delete('taxes/destroy', 'TaxController@massDestroy')->name('taxes.massDestroy');
    Route::resource('taxes', 'TaxController');

    // Product Seos
    Route::delete('product-seos/destroy', 'ProductSeoController@massDestroy')->name('product-seos.massDestroy');
    Route::resource('product-seos', 'ProductSeoController');

    // Tickets
    Route::delete('tickets/destroy', 'TicketController@massDestroy')->name('tickets.massDestroy');
    Route::post('tickets/media', 'TicketController@storeMedia')->name('tickets.storeMedia');
    Route::post('tickets/ckmedia', 'TicketController@storeCKEditorImages')->name('tickets.storeCKEditorImages');
    Route::resource('tickets', 'TicketController');

    // Ticket Messages
    Route::delete('ticket-messages/destroy', 'TicketMessageController@massDestroy')->name('ticket-messages.massDestroy');
    Route::post('ticket-messages/media', 'TicketMessageController@storeMedia')->name('ticket-messages.storeMedia');
    Route::post('ticket-messages/ckmedia', 'TicketMessageController@storeCKEditorImages')->name('ticket-messages.storeCKEditorImages');
    Route::resource('ticket-messages', 'TicketMessageController');

    // Ratings
    Route::delete('ratings/destroy', 'RatingController@massDestroy')->name('ratings.massDestroy');
    Route::resource('ratings', 'RatingController');

    // Shipments
    Route::delete('shipments/destroy', 'ShipmentController@massDestroy')->name('shipments.massDestroy');
    Route::post('shipments/media', 'ShipmentController@storeMedia')->name('shipments.storeMedia');
    Route::post('shipments/ckmedia', 'ShipmentController@storeCKEditorImages')->name('shipments.storeCKEditorImages');
    Route::resource('shipments', 'ShipmentController');

    // Payment Gateways
    Route::delete('payment-gateways/destroy', 'PaymentGatewayController@massDestroy')->name('payment-gateways.massDestroy');
    Route::resource('payment-gateways', 'PaymentGatewayController');

    // Payment Transactions
    Route::delete('payment-transactions/destroy', 'PaymentTransactionController@massDestroy')->name('payment-transactions.massDestroy');
    Route::resource('payment-transactions', 'PaymentTransactionController');

    // Refunds
    Route::delete('refunds/destroy', 'RefundController@massDestroy')->name('refunds.massDestroy');
    Route::resource('refunds', 'RefundController');

    // Settings
    Route::delete('settings/destroy', 'SettingController@massDestroy')->name('settings.massDestroy');
    Route::post('settings/media', 'SettingController@storeMedia')->name('settings.storeMedia');
    Route::post('settings/ckmedia', 'SettingController@storeCKEditorImages')->name('settings.storeCKEditorImages');
    Route::resource('settings', 'SettingController');

    // Vouchers
    Route::delete('vouchers/destroy', 'VoucherController@massDestroy')->name('vouchers.massDestroy');
    Route::resource('vouchers', 'VoucherController');

    // Wishlists
    Route::delete('wishlists/destroy', 'WishlistController@massDestroy')->name('wishlists.massDestroy');
    Route::resource('wishlists', 'WishlistController');

    // Stores
    Route::delete('stores/destroy', 'StoreController@massDestroy')->name('stores.massDestroy');
    Route::post('stores/media', 'StoreController@storeMedia')->name('stores.storeMedia');
    Route::post('stores/ckmedia', 'StoreController@storeCKEditorImages')->name('stores.storeCKEditorImages');
    Route::resource('stores', 'StoreController');

    // Approvals
    Route::delete('approvals/destroy', 'ApprovalController@massDestroy')->name('approvals.massDestroy');
    Route::resource('approvals', 'ApprovalController');

    Route::get('global-search', 'GlobalSearchController@search')->name('globalSearch');
    Route::get('messenger', 'MessengerController@index')->name('messenger.index');
    Route::get('messenger/create', 'MessengerController@createTopic')->name('messenger.createTopic');
    Route::post('messenger', 'MessengerController@storeTopic')->name('messenger.storeTopic');
    Route::get('messenger/inbox', 'MessengerController@showInbox')->name('messenger.showInbox');
    Route::get('messenger/outbox', 'MessengerController@showOutbox')->name('messenger.showOutbox');
    Route::get('messenger/{topic}', 'MessengerController@showMessages')->name('messenger.showMessages');
    Route::delete('messenger/{topic}', 'MessengerController@destroyTopic')->name('messenger.destroyTopic');
    Route::post('messenger/{topic}/reply', 'MessengerController@replyToTopic')->name('messenger.reply');
    Route::get('messenger/{topic}/reply', 'MessengerController@showReply')->name('messenger.showReply');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }
});
