@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.tax.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.taxes.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.tax.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tax.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="tax_percentage">{{ trans('cruds.tax.fields.tax_percentage') }}</label>
                <input class="form-control {{ $errors->has('tax_percentage') ? 'is-invalid' : '' }}" type="number" name="tax_percentage" id="tax_percentage" value="{{ old('tax_percentage', '') }}" step="1" required>
                @if($errors->has('tax_percentage'))
                    <span class="text-danger">{{ $errors->first('tax_percentage') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tax.fields.tax_percentage_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection