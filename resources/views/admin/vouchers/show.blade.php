@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.voucher.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.vouchers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.voucher.fields.id') }}
                        </th>
                        <td>
                            {{ $voucher->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.voucher.fields.title') }}
                        </th>
                        <td>
                            {{ $voucher->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.voucher.fields.category') }}
                        </th>
                        <td>
                            {{ $voucher->category->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.voucher.fields.percentage') }}
                        </th>
                        <td>
                            {{ $voucher->percentage }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.voucher.fields.expired_at') }}
                        </th>
                        <td>
                            {{ $voucher->expired_at }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.voucher.fields.max_usage') }}
                        </th>
                        <td>
                            {{ $voucher->max_usage }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.voucher.fields.store') }}
                        </th>
                        <td>
                            {{ $voucher->store->title ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.vouchers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection