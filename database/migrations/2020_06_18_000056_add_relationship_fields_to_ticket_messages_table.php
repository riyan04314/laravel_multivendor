<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTicketMessagesTable extends Migration
{
    public function up()
    {
        Schema::table('ticket_messages', function (Blueprint $table) {
            $table->unsignedInteger('ticket_id')->nullable();
            $table->foreign('ticket_id', 'ticket_fk_1467237')->references('id')->on('tickets');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_fk_1467239')->references('id')->on('users');
        });
    }
}
