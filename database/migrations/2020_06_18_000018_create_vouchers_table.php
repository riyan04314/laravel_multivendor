<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('percentage')->nullable();
            $table->date('expired_at')->nullable();
            $table->string('max_usage')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
