<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('message');
            $table->string('status');
            $table->integer('ratings')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
