<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToAttributesTable extends Migration
{
    public function up()
    {
        Schema::table('attributes', function (Blueprint $table) {
            $table->unsignedInteger('type_id')->nullable();
            $table->foreign('type_id', 'type_fk_1467169')->references('id')->on('types');
        });
    }
}
