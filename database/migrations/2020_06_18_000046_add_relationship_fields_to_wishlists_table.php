<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToWishlistsTable extends Migration
{
    public function up()
    {
        Schema::table('wishlists', function (Blueprint $table) {
            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id', 'product_fk_1470593')->references('id')->on('products');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_fk_1470594')->references('id')->on('users');
        });
    }
}
