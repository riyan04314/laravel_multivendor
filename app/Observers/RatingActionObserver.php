<?php

namespace App\Observers;

use App\Notifications\DataChangeEmailNotification;
use App\Rating;
use Illuminate\Support\Facades\Notification;

class RatingActionObserver
{
    public function created(Rating $model)
    {
        $data  = ['action' => 'created', 'model_name' => 'Rating'];
        $users = \App\User::whereHas('roles', function ($q) {
            return $q->where('title', 'Admin');
        })->get();
        Notification::send($users, new DataChangeEmailNotification($data));
    }
}
