<?php

namespace App\Observers;

use App\Notifications\DataChangeEmailNotification;
use App\PaymentTransaction;
use Illuminate\Support\Facades\Notification;

class PaymentTransactionActionObserver
{
    public function created(PaymentTransaction $model)
    {
        $data  = ['action' => 'created', 'model_name' => 'PaymentTransaction'];
        $users = \App\User::whereHas('roles', function ($q) {
            return $q->where('title', 'Admin');
        })->get();
        Notification::send($users, new DataChangeEmailNotification($data));
    }
}
