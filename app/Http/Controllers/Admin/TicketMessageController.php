<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyTicketMessageRequest;
use App\Http\Requests\StoreTicketMessageRequest;
use App\Http\Requests\UpdateTicketMessageRequest;
use App\Ticket;
use App\TicketMessage;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class TicketMessageController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('ticket_message_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ticketMessages = TicketMessage::all();

        return view('admin.ticketMessages.index', compact('ticketMessages'));
    }

    public function create()
    {
        abort_if(Gate::denies('ticket_message_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tickets = Ticket::all()->pluck('status', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.ticketMessages.create', compact('tickets', 'users'));
    }

    public function store(StoreTicketMessageRequest $request)
    {
        $ticketMessage = TicketMessage::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $ticketMessage->id]);
        }

        return redirect()->route('admin.ticket-messages.index');
    }

    public function edit(TicketMessage $ticketMessage)
    {
        abort_if(Gate::denies('ticket_message_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tickets = Ticket::all()->pluck('status', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $ticketMessage->load('ticket', 'user');

        return view('admin.ticketMessages.edit', compact('tickets', 'users', 'ticketMessage'));
    }

    public function update(UpdateTicketMessageRequest $request, TicketMessage $ticketMessage)
    {
        $ticketMessage->update($request->all());

        return redirect()->route('admin.ticket-messages.index');
    }

    public function show(TicketMessage $ticketMessage)
    {
        abort_if(Gate::denies('ticket_message_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ticketMessage->load('ticket', 'user');

        return view('admin.ticketMessages.show', compact('ticketMessage'));
    }

    public function destroy(TicketMessage $ticketMessage)
    {
        abort_if(Gate::denies('ticket_message_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ticketMessage->delete();

        return back();
    }

    public function massDestroy(MassDestroyTicketMessageRequest $request)
    {
        TicketMessage::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('ticket_message_create') && Gate::denies('ticket_message_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new TicketMessage();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
