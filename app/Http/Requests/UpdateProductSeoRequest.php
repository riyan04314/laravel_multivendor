<?php

namespace App\Http\Requests;

use App\ProductSeo;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateProductSeoRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('product_seo_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'language_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
